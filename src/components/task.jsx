import React, { Component } from 'react';

import Paper from "@material-ui/core/Paper";

class Task extends Component {
    constructor(props) {
      super(props)
      this.taskDrag = this.taskDrag.bind(this)
    }

    taskDrag(e) {
      e.dataTransfer.setData("application/task", this.props.id);
    }

    render() {
        return (
          <Paper draggable onDragStart={this.taskDrag} elevation={2}>
            <p><strong>{this.props.name}</strong></p>
            <p>{this.props.desc}</p>
          </Paper>
        );
    }
}

export default Task;