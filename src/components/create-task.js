import React from "react"

import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import AddIcon from "@material-ui/icons/Add";

class CreateTask extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      desc: ""
    }
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleName = this.handleName.bind(this);
    this.handleDesc = this.handleDesc.bind(this);
  }

  render() {
    return (
      <div id="create-task">
        <Typography variant="h5" component="h3">
          Create Task
        </Typography>
        <form onSubmit={this.handleSubmit}>
          <TextField
            id="name"
            label="Task Name"
            onChange={this.handleName}
            value={this.state.name}
          />
          <TextField
            id="name"
            label="Task Description"
            onChange={this.handleDesc}
            value={this.state.desc}
          />
          <Button
            type="submit"
            variant="contained"
            color="primary"
            size="small"
          >
            <AddIcon />
            Add Task
          </Button>
        </form>
      </div>
    );
  }

  handleName(e) {
    this.setState({
      name: e.target.value
    })
  }
  handleDesc(e) {
    this.setState({
      desc: e.target.value,
    });
  }

  handleSubmit(e) {
    const newTask = this.state
    this.props.onAddTask(newTask);
    e.preventDefault();
  }
}

export default CreateTask